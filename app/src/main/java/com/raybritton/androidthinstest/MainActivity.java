package com.raybritton.androidthinstest;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.widget.EditText;

import com.google.android.things.pio.PeripheralManagerService;
import com.raybritton.androidthinstest.sensors.GpioWatcher;

public class MainActivity extends Activity implements GpioWatcher.GpioListener {

    private EditText output;
    private StringBuilder builder = new StringBuilder();
    private GpioWatcher button;
    private GpioSender blue;
    private GpioSender red;
    private boolean blueIsActive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);
        output = (EditText) findViewById(R.id.output);

        PeripheralManagerService manager = new PeripheralManagerService();

        print("Ready");

        button = new GpioWatcher(manager, "BCM4", this);
        red = new GpioSender(manager, "BCM21");
        blue = new GpioSender(manager, "BCM20");
    }

    @Override
    protected void onStart() {
        super.onStart();
        button.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        button.stop();
    }

    private void print(String text) {
        builder.append(text).append('\n');
        output.setText(builder);
    }

    @Override
    public void onValueChange(GpioWatcher watcher, boolean value) {
        if (value) {
            blueIsActive = !blueIsActive;
            blue.set(blueIsActive);
            red.set(!blueIsActive);
            print("Toggled: " + System.currentTimeMillis());
        }
    }

    @Override
    public void onError(GpioWatcher watcher, int error) {
        print("Error: " + error);
    }
}
