package com.raybritton.androidthinstest.sensors;

import com.google.android.things.pio.PeripheralManagerService;
import com.google.android.things.pio.SpiDevice;
import com.raybritton.androidthinstest.Sensor;

import java.io.IOException;
import java.nio.ByteBuffer;

import timber.log.Timber;

//BCM25 display is pressed
public class SpiWatcher implements Sensor {
    public interface SpiListener {
        int ERROR_READ = 1;
        int ERROR_INIT = 2;
        void onValueChange(SpiWatcher watcher, String value);
        void onError(SpiWatcher watcher, int error);
    }

    private final String name;
    private final SpiListener listener;

    private SpiDevice spi;
    private SpiThread thread;

    public SpiWatcher(PeripheralManagerService service, final String name, final SpiListener listener) {
        this.name = name;
        this.listener = listener;
        try {
            spi = service.openSpiDevice(name);
            spi.setBitsPerWord(8);
            spi.setBitJustification(false);
            spi.setFrequency(2000000);
            spi.setMode(SpiDevice.MODE0);
        } catch (IOException e) {
            Timber.e(e, "SPI Start: " + name);
            spi = null;
        }
    }

    public String getName() {
        return name;
    }

    @Override
    public void start() {
        if (spi == null) {
            listener.onError(this, SpiListener.ERROR_INIT);
            return;
        }
        if (thread != null) {
            thread.isRunning = false;
        }
        thread = new SpiThread(this, spi, listener);
        thread.start();
    }

    @Override
    public void stop() {
        if (thread != null) {
            thread.isRunning = false;
        }
    }

    private static final int X = 0x91;
    private static final int Y = 0xD1;
    private static final int Z = 0xC1;

    private static class SpiThread extends Thread implements Runnable {
        private SpiWatcher watcher;
        private SpiDevice spi;
        private SpiListener listener;
        boolean isRunning;

        private String lastMessage = "";

        public SpiThread(final SpiWatcher watcher, final SpiDevice spi, final SpiListener listener) {
            this.watcher = watcher;
            this.spi = spi;
            this.listener = listener;
            this.isRunning = true;
        }

        @Override
        public void run() {
            while(isRunning) {
                try {
                    int x = send(X);
                    int y = send(Y);
                    int z = send(Z);
                    String msg = x+","+y+","+z;
                    if (!msg.equals(lastMessage)) {
                        lastMessage = msg;
                        listener.onValueChange(watcher, msg);
                    }
                } catch (IOException e) {
                    Timber.e(e, "SPI Read: " + watcher.getName());
                    listener.onError(watcher, SpiListener.ERROR_READ);
                }
            }
        }

        private int send(int value) throws IOException {
            byte[] tx = ByteBuffer.allocate(2).putShort((short) value).array();
            byte[] rx = new byte[2];
            spi.transfer(tx, rx, 2);
            return rx[0] & 0xFF | rx[1] & 0xFF;
        }
    }
}
