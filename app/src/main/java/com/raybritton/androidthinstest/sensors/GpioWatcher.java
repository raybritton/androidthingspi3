package com.raybritton.androidthinstest.sensors;

import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.GpioCallback;
import com.google.android.things.pio.PeripheralManagerService;
import com.raybritton.androidthinstest.Sensor;

import java.io.IOException;

import timber.log.Timber;


public class GpioWatcher extends GpioCallback implements Sensor {
    public interface GpioListener {
        int ERROR_READ = 1;
        int ERROR_INIT = 2;
        void onValueChange(GpioWatcher watcher, boolean value);
        void onError(GpioWatcher watcher, int error);
    }

    private final String name;
    private final GpioListener listener;

    private Gpio gpio;

    public GpioWatcher(final PeripheralManagerService manager, final String name, final GpioListener listener) {
        this.name = name;
        this.listener = listener;
        try {
            gpio = manager.openGpio(name);
            gpio.setDirection(Gpio.DIRECTION_IN);
            gpio.setActiveType(Gpio.ACTIVE_LOW);
            gpio.setEdgeTriggerType(Gpio.EDGE_BOTH);
        } catch (IOException e) {
            gpio = null;
            Timber.e(e, "GPIO Start: " + name);
        }
    }

    public String getName() {
        return name;
    }

    public void start() {
        if (gpio == null) {
            listener.onError(this, GpioListener.ERROR_INIT);
            return;
        }
        try {
            gpio.registerGpioCallback(callback);
            callback.onGpioEdge(gpio);
        } catch (IOException e) {
            Timber.e(e, "GPIO Register: " + name);
        }
    }

    public void stop() {
        if (gpio != null) {
            gpio.unregisterGpioCallback(callback);
        }
    }

    private GpioCallback callback = new GpioCallback() {
        @Override
        public boolean onGpioEdge(final Gpio gpio) {
            try {
                listener.onValueChange(GpioWatcher.this, gpio.getValue());
            } catch (IOException e) {
                listener.onError(GpioWatcher.this, GpioListener.ERROR_READ);
                Timber.e(e, "GPIO Read: " + name);
            }
            return true;
        }

        @Override
        public void onGpioError(final Gpio gpio, final int error) {
            listener.onError(GpioWatcher.this, error);
        }
    };
}
