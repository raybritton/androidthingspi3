package com.raybritton.androidthinstest.sensors;

import com.google.android.things.pio.PeripheralManagerService;
import com.google.android.things.pio.UartDevice;
import com.google.android.things.pio.UartDeviceCallback;
import com.raybritton.androidthinstest.Sensor;

import java.io.IOException;

import timber.log.Timber;


public class UartWatcher implements Sensor {
    public interface UartListener {
        int ERROR_READ = 1;
        int ERROR_INIT = 2;
        void onValue(UartWatcher watcher, byte byt);
        void onError(UartWatcher watcher, int error);
    }
    private final String name;
    private final UartListener listener;
    private UartDevice uart;

    public UartWatcher(final PeripheralManagerService manager, final String name, final UartListener listener) {
        this.name = name;
        this.listener = listener;
        try {
            this.uart = manager.openUartDevice(name);
            uart.setBaudrate(115200); //9600, 19200, 38400, 57600, 115200, 921600
            uart.setDataSize(8);
            uart.setParity(UartDevice.PARITY_NONE);
            uart.setStopBits(1);
        } catch (IOException e) {
            uart = null;
            Timber.e(e, "UART Start: " + name);
        }
    }

    public String getName() {
        return name;
    }

    @Override
    public void start() {
        if (uart == null) {
            listener.onError(this, UartListener.ERROR_INIT);
            return;
        }
        try {
            uart.registerUartDeviceCallback(callback);
        } catch (IOException e) {
            Timber.e(e, "UART Register: " + name);
        }
    }

    @Override
    public void stop() {
        if (uart != null) {
            uart.unregisterUartDeviceCallback(callback);
        }
    }

    private UartDeviceCallback callback = new UartDeviceCallback() {
        @Override
        public boolean onUartDeviceDataAvailable(final UartDevice uart) {
            try {
                readUartBuffer(uart);
            } catch (IOException e) {
                listener.onError(UartWatcher.this, UartListener.ERROR_READ);
                Timber.e(e, "UART Read: " + name);
            }
            return true;
        }

        @Override
        public void onUartDeviceError(final UartDevice uart, final int error) {
            listener.onError(UartWatcher.this, error);
        }
    };

    private void readUartBuffer(UartDevice uart) throws IOException {
        final int maxCount = 1;
        byte[] buffer = new byte[maxCount];

        int count;
        while ((count = uart.read(buffer, buffer.length)) > 0) {
            for(int i = 0; i < count; i++) {
                listener.onValue(this, buffer[i]);
            }
        }
    }
}
