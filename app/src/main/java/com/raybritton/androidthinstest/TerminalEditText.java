package com.raybritton.androidthinstest;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.InputType;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.EditText;
import android.widget.TextView;

public class TerminalEditText extends EditText{
    public TerminalEditText(Context context) {
        super(context);
        init();
    }

    public TerminalEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TerminalEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public TerminalEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, type);
        int len = text.length();
        setSelection(len);
    }

    private void init() {
        int padding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
        setBackgroundColor(Color.BLACK);
        setTextColor(Color.GREEN);
        setTypeface(Typeface.MONOSPACE);
        setPadding(padding, padding, padding, padding);
        setTextSize(TypedValue.COMPLEX_UNIT_SP, 26);
        setCursorVisible(true);
        setGravity(Gravity.START | Gravity.TOP);
    }
}
