package com.raybritton.androidthinstest;

public interface Sensor {
    void start();
    void stop();
}
