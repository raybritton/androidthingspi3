package com.raybritton.androidthinstest.examples;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.widget.EditText;

import com.google.android.things.pio.PeripheralManagerService;
import com.raybritton.androidthinstest.R;
import com.raybritton.androidthinstest.sensors.GpioWatcher;

/**
 * 330 power - j10
 * button 4 e10, 3 h10
 * wires:
 *  BCM21 - a10
 *  ground - G
 *  power - 5V
 *  ground - j12
 */
public class ButtonActivity extends Activity implements GpioWatcher.GpioListener {

    private EditText output;
    private GpioWatcher watcher;
    private StringBuilder builder = new StringBuilder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_button);
        output = (EditText) findViewById(R.id.output);

        print("Ready");

        PeripheralManagerService manager = new PeripheralManagerService();

        watcher = new GpioWatcher(manager, "BCM21", this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        watcher.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        watcher.stop();
    }

    @Override
    public void onValueChange(GpioWatcher watcher, boolean value) {
        if (value) {
            print("Pressed");
        } else {
            print("Released");
        }
    }

    @Override
    public void onError(GpioWatcher watcher, int error) {
        print("Error: " + error);
    }

    private void print(String text) {
        builder.append(text).append('\n');
        output.setText(builder);
    }
}
