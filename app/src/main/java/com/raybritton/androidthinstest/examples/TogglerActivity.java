package com.raybritton.androidthinstest.examples;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.widget.EditText;

import com.google.android.things.pio.PeripheralManagerService;
import com.raybritton.androidthinstest.GpioSender;
import com.raybritton.androidthinstest.R;
import com.raybritton.androidthinstest.sensors.GpioWatcher;

/**
 * BB1
 * 330 power - j10
 * button 4 e10, 3 h10
 * wires:
 *  BCM4 - c10
 *  ground - G
 *  power - 3.3V
 *  ground - j12
 *
 * BB2
 * 330 power1 - e6
 * led pos a6, neg ground1
 * 330 h20 - ground2
 * led pos power2, neg j20
 * wires:
 *  BCM21 - power1
 *  BCM20 - power2
 *  ground1 - G
 *  ground2 - G
 */
public class TogglerActivity extends Activity implements GpioWatcher.GpioListener {

    private EditText output;
    private StringBuilder builder = new StringBuilder();
    private GpioWatcher button;
    private GpioSender blue;
    private GpioSender red;
    private boolean blueIsActive;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_toggler);
        output = (EditText) findViewById(R.id.output);

        PeripheralManagerService manager = new PeripheralManagerService();

        print("Ready");

        button = new GpioWatcher(manager, "BCM4", this);
        red = new GpioSender(manager, "BCM21");
        blue = new GpioSender(manager, "BCM20");
    }

    @Override
    protected void onStart() {
        super.onStart();
        button.start();
    }

    @Override
    protected void onStop() {
        super.onStop();
        button.stop();
    }

    private void print(String text) {
        builder.append(text).append('\n');
        output.setText(builder);
    }

    @Override
    public void onValueChange(GpioWatcher watcher, boolean value) {
        if (value) {
            blueIsActive = !blueIsActive;
            blue.set(blueIsActive);
            red.set(!blueIsActive);
            print("Toggled: " + System.currentTimeMillis());
        }
    }

    @Override
    public void onError(GpioWatcher watcher, int error) {
        print("Error: " + error);
    }
}
