package com.raybritton.androidthinstest.examples;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.widget.EditText;

import com.google.android.things.pio.PeripheralManagerService;
import com.raybritton.androidthinstest.GpioSender;
import com.raybritton.androidthinstest.R;

/**
 * LED pos a1, neg a4
 * 330 power - c1
 * wires:
 *  ground - d4
 *  power - BCM21
 *  ground - G
 */
public class LedActivity extends Activity {

    private EditText output;
    private StringBuilder builder = new StringBuilder();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_led);
        output = (EditText) findViewById(R.id.output);

        PeripheralManagerService manager = new PeripheralManagerService();

        GpioSender sender = new GpioSender(manager, "BCM21");
        print("Starting");
        sender.set(true);
        print("Done");
    }


    private void print(String text) {
        builder.append(text).append('\n');
        output.setText(builder);
    }
}
