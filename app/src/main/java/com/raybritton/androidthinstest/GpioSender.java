package com.raybritton.androidthinstest;

import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.PeripheralManagerService;

import java.io.IOException;

import timber.log.Timber;

public class GpioSender {
    private Gpio gpio;
    private String name;
    private boolean lastValue;

    public GpioSender(final PeripheralManagerService manager, final String name) {
        this.name = name;
        try {
            gpio = manager.openGpio(name);
            gpio.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
        } catch (IOException e) {
            gpio = null;
            Timber.e(e, "GPIO Start: " + name);
        }
    }

    public String getName() {
        return name;
    }

    public void set(boolean on) {
        try {
            gpio.setValue(on);
            lastValue = on;
        } catch (IOException e) {
            e.printStackTrace();
            Timber.e(e, "GPIO failed set: " + name);
        }
    }

    public void toggle() {
        set(!lastValue);
    }
}
